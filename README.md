
# Activity Logging Program


## Deskripsi Aplikasi 
Program ini menggunakan metode Interprocess Communication menggunakan Socket Programming. Program dapat mencatat semua aktivitas yang dilakukan oleh client. IP address client akan ditampilkan di server. Client dapat melihat log activity yang dilakukan dan dapat melihat log aktivitas untuk bulan dan tahun yang diinginkan.

## Pembagian Tugas Anggota
Hana Rifdah Sakinah
- Membuat server dan client sederhana
- Menampilkan log activity di client
- Menambahkan seluruh command
- Membuat dokumentasi program

Farizan Cesario
- Update server
- Update log dan server
- Update filter untuk log
## Prosedur Instalasi
1. Run program "cots_server.py"
2. Server is listening
3. Run program "cots_client.py"
4. Client menginputkan command (ping, cd, ls, atau log)
5. log digunakan untuk melihat log aktivitas yang dilakukan berdasarkan bulan dan tahun yang diinginkan.
6. Ketika client menginputkan command maka command akan dikirimkan ke server dan disimpan ke dalam file "log.txt".
7. Ketika Client menginputkan bulan dan tahun yang diinginkan maka server akan mengirimkan log aktivitas yang ada pada bulan dan tahun yang diinginkan.
