import socket #import lib socket
import re #import lib reguler expression
import datetime #import date and time

ip = "192.168.0.102" #definisikan ip yang dipakai

port = 12346 #definisikan port yang dipakai

buff_size = 1024 #definisikan Ukuran Buffer

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #socket bertipe TCP
print("udah connect ke ", ip) #memunculkan ip address dari client

s.bind((ip, port)) #mengikat ip dan port 

s.listen(5) # melakukan listen

ip = [] #bikin array buat ip 
command = ["cd", "ping", "ls"] #membuat array command yang isinya cd, ping, ls


while 1: #mulai perulangan
    
    c, addr = s.accept() #connect ke address 
    ip.append(addr[0]) #mengeluarkan ip address nya saja
    print("Address: ", ip[0]) #nampilin ip address di server
    while 1:
        get = c.recv(buff_size).decode() #receive pesan sebesar buffer size nya yang udah di decode
        match1 = re.search("^log", get) #mencari log menggunakan regex
        match2 = re.search("^cd", get) #mencari cd menggunakan regex
        match3 = re.search("^ls", get) #mencari ls menggunakan regex
        match4 = re.search("^ping", get) #mencari ping menggunakan regex
        if match1: #jika match1 bertemu log
            x = open('log.txt', "rb") #membuka file log.txt lalu dibaca
            try:
                isi = x.read(buff_size) #ngebaca isi file log.txt
                while isi != b'': #selama isi ga kosong
                    c.send(isi) #mengirim isi ke client
                    isi=x.read(buff_size) #membaca isi file log.txt lagi
            finally:
                x.close() #file nya ditutup jika sudah selesai
        elif match2 or match3 or match4: #kalo yang ketemu match2, match3, atau match 4
            x = open('log.txt', "a+") #membuka file log.txt trus menambahkan datanya ke file tersebut
            x.write(datetime.datetime.now().strftime ("%D %H:%M:%S ") + get + "\n") #Menulis aksi yang dilakukan dan waktunya
            x.close() #kalo sudah file nya ditutup
        else: #kalo selain di atas
            break #break
s.close() #socket ditutup
