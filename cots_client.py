import socket #import lib socket

ip = "localhost" #definisikan ip yang dipakai

port = 12346 #definisikan port yang dipakai

buff_size = 1024 #definisikan ukuran buffer untuk menerima pesan

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #socket bertipe TCP

s.connect((ip, port)) #connect ke server dengan parameter IP dan port yang telah didefinisikan

while 1: #perulangan dimulai
    i = input(">") #masukan input command yang ingin diinputkan
    s.send(i.encode()) #mengirim command yang di encode ke server
    if i == "log": #Jika user meng-inputkan log
        data = s.recv(buff_size).decode() #akan nerima data dari sever sebesar buffer size yang di decode
        m = str(input("Masukkan bulan(angka) : ")) #user meng-inputkan bulan (berbentuk angka)
        y = str(input("Masukkan tahun(2 digit dibelakang) : ")) #user meng-inputkan tahun (berbentuk 2 digit di belakang)
        l = data.split("\n") #variabel l adalah data yang dipisah selama bertemu ("\n") agar dapat menjadi array di masing-masing baris
        hasil = [] #membuat array bernama hasil
        x = 0 #membuat variabel untuk iterasi
        while x < len(l): #Selama x kurang dari panjang l
            if l[x][:2] == m and l[x][6:8] == y: #jika x di panjang l dari index ke 0 sampai index ke 2 sama dengan m dan x dari index ke 6 sampai ke 8 sama dengan y 
                hasil.append(l[x]) #nilai dari x akan di append atau dimasukkan ke array hasil
            x += 1 #loop
        x = 0 #membuat variabel untuk iterasi
        while x < len(hasil): #selama x kurang dari panjang dari array hasil
            print(hasil[x]) #tampilkan isi dari hasil
            x += 1 #loop
s.close() #socket ditutup